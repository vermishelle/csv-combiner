require 'smarter_csv' # для чтения
require 'csv' # для записи

$a_must_field = :ФИО

if ARGV == []
  puts "Добавьте одно или несколько имен CSV-файлов в качестве параметра."
  exit
end

options = {col_sep: ';'}

csv_array_of_hashes = Array.new

ARGV.each do |file|
  csv_array_of_hashes.push(*SmarterCSV.process(file, options))
end

# csv_hash - массив, каждая строчка - хэш с названием колонок в качестве ключей
# и значениями ячеек в зачестве значений хэша

all_keys = []

csv_array_of_hashes.each do |hash|
  all_keys.push(*hash.keys)
end

unique_keys = all_keys.uniq!

# попробую тупо собрать массив массивов,
# состоящий из заголовков в первом елементе (типа ряду таблицы), и далее значений

result_array_of_arrays = []

# первый ряд в массиве массивов - названия столбцов
result_array_of_arrays.push(unique_keys)

csv_array_of_hashes.each do |original_hash|

  temp = []

  unique_keys.each do |unique_key|
    temp.push(original_hash[unique_key])
  end

  result_array_of_arrays.push(temp)

end

# запись результата в CSV-файл
CSV.open("output-all.csv", "wb") do |csv|
  result_array_of_arrays.each do |row|
    csv << row
  end
end

def conflicting(hash_to_be_filled, hash_to_fill_by)

  if hash_to_be_filled == hash_to_fill_by
    return hash_to_be_filled
  end

  conflicting_status = false

  hash_to_be_filled.each_pair do |key, value|
    unless hash_to_fill_by[key] == value or hash_to_fill_by[key] == nil or value == nil
      conflicting_status = true
      return conflicting_status # если нашли хоть один конфликт - выходим
    end
  end

  return conflicting_status # должно быть false
end

def fill_missing_fields(hash_to_be_filled, hash_to_fill_by)

  # если нет конфликта (взаимопротиворечащих данных), имена совпадают и поле ФИО не пустое

  if !conflicting(hash_to_be_filled, hash_to_fill_by) and (hash_to_be_filled[$a_must_field] == hash_to_fill_by[$a_must_field]) and (hash_to_fill_by[$a_must_field] != nil)
    if hash_to_be_filled == hash_to_fill_by
      return hash_to_be_filled
    end

    puts "=========================="
    puts "заполняю недостающие поля в"
    puts hash_to_be_filled.inspect
    puts "из"
    puts hash_to_fill_by.inspect
    puts "=========================="
    hash_to_be_filled.merge!(hash_to_fill_by) do |key, oldval, newval|
      next newval if oldval == nil
      next oldval if newval == nil
      next newval if newval == oldval
    end
    puts "получилось:"
    puts hash_to_be_filled.inspect
    puts "=========================="
    return hash_to_be_filled
  end

  # если поля конфликтуют, или не совпадают имена, или ФИО пустое - возвращаем как есть
  return hash_to_be_filled

end

## надо проверить эту функцию!
def array_already_has_the_same_hash(array, hash)
  if array.include?(hash)
    return true
  end
end

csv_array_of_hashes_with_filled_fields = []

csv_array_of_hashes.each do |outer_hash|
  csv_array_of_hashes.each do |inner_hash|
    hash_with_max_info = fill_missing_fields(outer_hash, inner_hash)
    unless array_already_has_the_same_hash(csv_array_of_hashes_with_filled_fields, hash_with_max_info)
      csv_array_of_hashes_with_filled_fields.push(hash_with_max_info)
    end
  end
end


result_array_of_arrays_filled = []

# первый ряд в массиве массивов - названия столбцов
result_array_of_arrays_filled.push(unique_keys)

csv_array_of_hashes_with_filled_fields.each do |original_hash|

  temp = []

  unique_keys.each do |unique_key|
    temp.push(original_hash[unique_key])
  end

  unless original_hash[$a_must_field] == nil
    puts "Помещаю в результирующий массив #{temp.inspect}"
    result_array_of_arrays_filled.push(temp)
    puts "==="
  end

end

# запись результата в CSV-файл
CSV.open("output-combined.csv", "wb") do |csv|
  result_array_of_arrays_filled.each do |row|
    csv << row
  end
end
